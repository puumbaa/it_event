package servlets;

import services.EventService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Arrays;

@WebServlet("/file-download")
public class FileDownloadServlet extends HttpServlet {


    private EventService eventService;

    private static final String PATH = "C:/Users/user/IdeaProjects/ItEvent/src/main/webapp/images/%s";

    @Override
    public void init(ServletConfig config) throws ServletException {
         ServletContext context = config.getServletContext();
         eventService = (EventService) context.getAttribute("eventService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filename = request.getParameter("filename");
        ServletOutputStream outputStream = response.getOutputStream();
        FileInputStream fin = new FileInputStream(String.format(PATH,filename));
        BufferedInputStream bis = new BufferedInputStream(fin);
        BufferedOutputStream bos = new BufferedOutputStream(outputStream);
        int ch = 0;
        while ((ch=bis.read())!=-1){
            bos.write(ch);
        }
        bis.close();
        fin.close();
        bos.close();
        outputStream.close();
    }
}
