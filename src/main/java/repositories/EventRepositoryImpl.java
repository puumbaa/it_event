package repositories;

import enums.EventStatus;
import models.Event;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

public class EventRepositoryImpl implements EventRepository {

    private JdbcTemplate jdbcTemplate;


    public EventRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    //language=sql
    private static final String SQL_INSERT =
            "insert into \"event\" (title, img_src, type, themes," +
                    " start_datetime, end_date, location, organizers, link, description, creator_id, status)" +
                    " values  (?,?,?,?,?,?,?,?,?,?,?,?)" +
                    " on conflict (title)" +
                    " do nothing";
    //language=sql
    private static final String SQL_DELETE_BY_ID = "delete from event where id = ?";
    //language=sql
    private static final String SQL_SELECT_ALL_BY_STATUS = "select * from \"event\" where status = ? and start_datetime > now() order by start_datetime";
    //language=sql
    private static final String SQL_SELECT_BY_ID = "select * from \"event\"where id = ?";
    //language=sql
    private static final String SQL_SELECT_ALL_BY_CREATOR_ID = "select * from event where creator_id = ?";
    //language=sql
    private static final String SQL_UPDATE_STATUS_BY_ID = "update event set status = ? where id = ?";
    //language=sql
    private static final String SQL_UPDATE_REJECTION_MESSAGE_BY_ID = "update event set rejection_message = ? where id = ?";
    //language=sql
    private static final String SQL_UPDATE_EVENT_BY_ID =
            "update event" +
                    " set title = ?," +
                    " type = ?," +
                    " themes = ?," +
                    " organizers = ?," +
                    " start_datetime = ?," +
                    " end_date = ?,"+
                    " location = ?," +
                    " link = ?," +
                    " description = ?," +
                    " img_src = ?" +
                    "where id = ?";

    private final RowMapper<Event> eventRowMapper = (row, rowNum) -> {
        final Event event = new Event();
        final String end_date = row.getString("end_date");
        final Long event_id = Long.valueOf(row.getString("id"));

        event.setId(event_id);
        event.setTitle(row.getString("title"));
        event.setImageSource(row.getString("img_src"));
        event.setType(row.getString("type"));
        event.setThemes(row.getString("themes"));

        event.setStartDateTime(LocalDateTime.parse(row.getString("start_datetime"),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        if (end_date != null) {
            event.setEndDate(LocalDate.parse(end_date,DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        } else{
            event.setEndDate(null);
        }

        event.setLocation(row.getString("location"));
        event.setOrganizers(row.getString("organizers"));
        event.setLink(row.getString("link"));
        event.setDescription(row.getString("description"));
        event.setCreatorId(row.getLong("creator_id"));
        event.setEventStatus(EventStatus.valueOf(row.getString("status")));
        if (event.getEventStatus().equals(EventStatus.REJECTED)){
            event.setRejectionMessage(row.getString("rejection_message"));
        }
        return event;
    };

    @Override
    public boolean save(Event event) {
        final int affectedRows = jdbcTemplate.update(SQL_INSERT,
                event.getTitle(),
                event.getImageSource(),
                event.getType(),
                event.getThemes(),
                event.getStartDateTime(),
                event.getEndDate(),
                event.getLocation(),
                event.getOrganizers(),
                event.getLink(),
                event.getDescription(),
                event.getCreatorId(),
                event.getEventStatus().name());

        return affectedRows == 1;
    }

    @Override
    public List<Event> findAll() {
        return null;
    }


    public List<Event> findEventByStatus(EventStatus eventStatus) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_STATUS,eventRowMapper, eventStatus.name());
    }

    @Override
    public Optional<Event> findById(Long id) {
        return Optional.of(jdbcTemplate.query(SQL_SELECT_BY_ID, eventRowMapper, id).get(0));
    }

    @Override
    public List<Event> findAllByCreatorId(Long id) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_CREATOR_ID, eventRowMapper, id);
    }

    @Override
    public void changeStatus(EventStatus eventStatus, Long eventId) {
        jdbcTemplate.update(SQL_UPDATE_STATUS_BY_ID, eventStatus.name(),eventId);
    }

    @Override
    public void updateRejectionMessage(String message,Long eventId) {
        jdbcTemplate.update(SQL_UPDATE_REJECTION_MESSAGE_BY_ID,message,eventId);
    }

    @Override
    public void updateEvent(String title, String type, String themes, String organizers, LocalDateTime startDateTime, LocalDate endDate, String location, String link, String description, String imageSource,Long id) {
        jdbcTemplate.update(SQL_UPDATE_EVENT_BY_ID,title,type,themes,organizers,startDateTime,endDate,location,link,description,imageSource,id);
    }

    @Override
    public void deleteEventById(Long id) {
        jdbcTemplate.update(SQL_DELETE_BY_ID,id);
    }
}
