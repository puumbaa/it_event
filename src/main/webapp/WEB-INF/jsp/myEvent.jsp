<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${event.title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/clearfix.css">
    <link rel="stylesheet" href="../css/clearfix.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/nav.css">
    <link rel="stylesheet" href="../css/nav.css">
    <link rel="stylesheet" href="../css/eventPage.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/eventPage.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/footer.css">
    <link rel="stylesheet" href="../css/footer.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <style>
        .event-image {
            background-image: url("${event.imageSource}");
        }

        .control-buttons button:first-child {
            margin-right: 1vw;
        }
    </style>
</head>
<body>

<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel"
     aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="commentModalLabel">Комментарий к отказу</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p id="commentMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Назад</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
     aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Подтвердите действие</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Отменить действие будет невозможно</p>
            </div>
            <div class="modal-footer">

                <form action="<c:url value="/my-events"/>" method="post">
                    <input type="hidden" id="hiddenId">
                    <input type="hidden" name="isEventPage" value="true">
                    <button class="btn btn-danger">Удалить</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Назад</button>
                </form>
            </div>
        </div>
    </div>
</div>
<header class="header sticky-top bg-light">
    <div class="container">
        <nav id="navbar" class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="<c:url value="/main"/>">It event</a>


            <ul class="navbar-nav mr-auto mt-2 mt-lg-0" id="navbar-items" style="width: 35vw">


                <c:if test="${isAdmin==true || isModerator==true}">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/processing-events"/>">Проверка</a>
                    </li>
                </c:if>


                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/my-events">Мои мероприятия</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.request.contextPath}/addEvent">Добавить мероприятие</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/favourites"/>">Избранные</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">Рассылка</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/signIn"/>">Выйти</a>
                </li>

            </ul>
        </nav>
    </div>
</header>

<div class="wrapper">
    <div class="container" style="padding: 0 8vw;">
        <div class="main-content">

            <div class="event-image"></div>

            <div class="event-main-info">

                <h1 class="event-title">${event.title}</h1>

                <p class="lead event-eventStatus">Статус: ${event.eventStatus.translate()}</p>

                <p class="lead event-date">
                    Дата: ${event.getDateOfStart()}
                    <c:if test="${event.endDate!=null}"> - ${event.getDate(event.endDate)} </c:if>
                </p>

                <p class="lead event-time"> Время начала: ${event.getTimeOfStart()} </p>

                <p class="lead event-location">${event.location}</p>

                <p class="event-type lead">${event.type}
                    <c:if test="${event.themes!=null}"> / <span class="lead">Темы:</span> ${event.themes} </c:if>
                </p>

                <p class="organizers lead">Организатор: ${event.organizers}</p>

                <a href="${event.link}">Зарегистрироваться</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="description">
        <h2 class="description-title">Описание</h2>
        <p>${event.description}</p>

        <div class="control-buttons" style="display: flex; margin-top: 3vh">
            <c:if test="${event.isRejected()}">
                <button type="button" onclick="showCommentModal(this)" data-bs-toggle="modal" data-bs-target="#commentModal" class="btn btn-secondary" data-comment="${event.rejectionMessage}">
                    Посмотреть комментарий
                </button>
            </c:if>
            <div class="container" style="display: flex; justify-content: space-around; align-items: center;">
                <a href="${pageContext.request.contextPath}/edit-event?id=${event.id}">
                    <button type="submit" class="btn btn-primary">
                        Редактировать
                    </button>
                </a>
                <button class="btn btn-danger" data-bs-toggle="modal"
                        data-bs-target="#deleteModal" data-bs-id="${event.id}" onclick="setHiddenId(this)">
                    Удалить
                </button>
            </div>
        </div>
    </div>
</div>


<footer class="site-footer">
    <div class="container">

        <div class="row" style="justify-content: space-between;">
            <div class="col-sm-12 col-md-6">
                <h6>About</h6>
                <p class="text-justify">Scanfcode.com <i>CODE WANTS TO BE SIMPLE </i> is an initiative to help the
                    upcoming programmers with the code. Scanfcode focuses on providing the most efficient code or
                    snippets as the code wants to be simple. We will help programmers build up concepts in different
                    programming languages that include C, C++, Java, HTML, CSS, Bootstrap, JavaScript, PHP, Android,
                    SQL and Algorithm.</p>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Quick Links</h6>
                <ul class="footer-links">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Contribute</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by
                    <a href="#">It event</a>.
                </p>
            </div>

        </div>
    </div>
</footer>


<script>

    function showCommentModal(button) {
        document.getElementById('commentMessage').innerText = button.dataset.comment
    }

    function setHiddenId(button) {
        var id = button.dataset.id
        document.getElementById('hiddenId').setAttribute('value', id)
    }

</script>

</body>
</html>
