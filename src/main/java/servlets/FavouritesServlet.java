package servlets;

import models.Event;
import models.Favorites;
import services.EventService;
import services.FavouritesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/favourites")
public class FavouritesServlet extends HttpServlet {

    private FavouritesService favouritesService;
    private EventService eventService;
    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        favouritesService = (FavouritesService) context.getAttribute("favouritesService");
        eventService = (EventService) context.getAttribute("eventService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Favorites> favorites = favouritesService.findAll((Long) request.getAttribute("userId"));
        List<Event> events = new ArrayList<>();
        favorites.forEach(favorites1 -> {events.add(eventService.findById(favorites1.getEventId()).get());});

        request.setAttribute("events",events);
        request.getRequestDispatcher("/WEB-INF/jsp/favourites.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long userId = (Long) request.getAttribute("userId");
        Long eventId = Long.valueOf(request.getParameter("id"));
        boolean add = request.getParameter("add").equals("true");

        Favorites favorites = new Favorites(userId, eventId);
        if (add){
            favouritesService.save(favorites);
        }else{
            favouritesService.remove(favorites);

        }
    }
}
