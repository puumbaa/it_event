package services;

import models.Favorites;

import java.util.List;

public interface FavouritesService {
    void save(Favorites favorites);
    void remove(Favorites favorites);
    List<Favorites> findAll(Long userId);
    void removeByEventId(Long id);
    void removeByUserId(Long id);
}
