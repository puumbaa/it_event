package models;

import enums.EventStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {
    private Long id;
    private String title;
    private String imageSource;
    private String type;
    private String themes;
    private LocalDateTime startDateTime;
    private LocalDate endDate;
    private String location;
    private String organizers;
    private String link;
    private String description;
    private Long creatorId;
    private EventStatus eventStatus;
    private String rejectionMessage;
    private boolean isFavourite;

    public String getDateOfStart(){
        String[] datetime = startDateTime.toString().split("T");
        String date = datetime[0];
        return getDate(date);
    }
    public String getTimeOfStart(){
        return startDateTime.toString().split("T")[1];
    }
    public String getDate(String date){
        String[] dateSplit = date.split("-");
        String day = dateSplit[2];
        String month = dateSplit[1];
        String year = dateSplit[0];

        return day + "." + month + "." + year;
    }
    public String getImageName(){
        return imageSource.split("/")[2];
    }
    public boolean checkIsFavourite(){
        return isFavourite;
    }
    public boolean isRejected(){ return eventStatus.equals(EventStatus.REJECTED);}
}
