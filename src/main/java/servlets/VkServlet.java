package servlets;

import com.google.gson.Gson;
import com.vk.api.sdk.client.Lang;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.objects.users.responses.GetResponse;
import forms.VkForm;
import listeners.MyServletContextListener;
import models.UserRole;
import repositories.CookieRepository;
import repositories.UserRoleRepository;
import services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

@WebServlet("/vk")
public class VkServlet extends HttpServlet {

    private UserService userService;
    private Properties properties;
    private CookieRepository cookieRepository;
    private UserRoleRepository userRoleRepository;
    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        userRoleRepository = (UserRoleRepository) context.getAttribute("userRoleRepository");
        userService = (UserService) context.getAttribute("userService");
        properties = (Properties) context.getAttribute("properties");
        cookieRepository = (CookieRepository) config.getServletContext().getAttribute("cookieRepository");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient,new Gson(),5);

        String code = request.getParameter("code");

        UserAuthResponse authResponse;
        try {
            authResponse = vk.oAuth()
                    .userAuthorizationCodeFlow(Integer.valueOf(properties.getProperty("vk.app-id")),
                    properties.getProperty("vk.client-secret"),
                    properties.getProperty("vk.redirect-url"), code).execute();
        } catch (ApiException | ClientException e) {
            throw new IllegalArgumentException(e);
        }

        UserActor actor = new UserActor(authResponse.getUserId(), authResponse.getAccessToken());
        try {

            GetResponse account = vk.users()
                    .get(actor)
                    .userIds(actor.getId().toString())
                    .lang(Lang.RU)
                    .unsafeParam("v", "5.131")
                    .execute()
                    .get(0);

            String username = account.getLastName()+" "+account.getFirstName() + "#" + account.getId();
            String email = account.getEmail();

            if (!userService.findIdByUsername(username).isPresent()){
                userService.saveFromVk(new VkForm(username,email));
            }
            HttpSession session = request.getSession();
            String auth = UUID.randomUUID().toString();
            session.setAttribute("auth",auth);
            cookieRepository.save(auth,userService.findIdByUsername(username).get());
            userRoleRepository.save(new UserRole(userService.findIdByUsername(username).get(),1));
            response.sendRedirect("/main");
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
