package servlets;

import enums.EventStatus;
import models.Event;
import services.EventService;
import services.FileService;
import services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static utils.DataExtractor.*;

@WebServlet("/addEvent")
@MultipartConfig
public class AddEventServlet extends HttpServlet {
    private EventService eventService;
    private FileService fileService;
    private UserService userService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        final ServletContext context = config.getServletContext();

        eventService = (EventService) context.getAttribute("eventService");
        fileService = (FileService) context.getAttribute("fileService");
        userService = (UserService) context.getAttribute("userService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/addEvent.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String auth = request.getSession().getAttribute("auth").toString();
        Long creatorId = userService.findByCookie(auth).getId();

        String title = extractInputValue(request.getPart("title"));
        String type = extractInputValue(request.getPart("type"));
        String themes = extractInputValue(request.getPart("themes"));
        String organizers = extractInputValue(request.getPart("organizers"));
        String startDateTimeAsString = extractInputValue(request.getPart("startDateTime"));
        LocalDateTime startDateTime = LocalDateTime.parse(startDateTimeAsString, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")) ;
        String endDateAsString = extractInputValue(request.getPart("endDate"));
        LocalDate endDate = LocalDate.parse(endDateAsString,DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String location = extractInputValue(request.getPart("location"));
        String link = extractInputValue(request.getPart("link"));
        String description = extractInputValue(request.getPart("description"));

        Part part = request.getPart("file");
        String fileName = UUID.randomUUID() + "." + part.getContentType().split("/")[1];
        fileService.upload(fileName,part.getInputStream());

        eventService.save(
                Event.builder()
                        .title(title)
                        .type(type)
                        .themes(themes)
                        .organizers(organizers)
                        .startDateTime(startDateTime)
                        .endDate(endDate)
                        .location(location)
                        .link(link)
                        .description(description)
                        .imageSource("/images/"+fileName)
                        .eventStatus(EventStatus.PROCESSING)
                        .creatorId(creatorId)
                        .build()
        );
        List<Event> events = eventService.findAllByCreatorId(creatorId);

        request.setAttribute("events",events);
        request.getRequestDispatcher("/WEB-INF/jsp/myEvents.jsp").forward(request,response);
    }
}
