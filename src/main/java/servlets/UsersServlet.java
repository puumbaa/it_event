package servlets;

import enums.Roles;
import models.User;
import models.UserRole;
import repositories.CookieRepository;
import repositories.UserRoleRepository;
import services.EventService;
import services.FavouritesService;
import services.UserService;

import javax.servlet.FilterConfig;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {

    private static final int ADMIN_ROLE_ID = 3;
    private static final int MODERATOR_ROLE_ID = 2;

    private CookieRepository cookieRepository;

    private UserService userService;

    private UserRoleRepository userRoleRepository;

    private FavouritesService favouritesService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        ServletContext context = servletConfig.getServletContext();
        userService = (UserService) context.getAttribute("userService");
        userRoleRepository = (UserRoleRepository) context.getAttribute("userRoleRepository");
        cookieRepository = (CookieRepository) context.getAttribute("cookieRepository");
        favouritesService = (FavouritesService) context.getAttribute("favouritesService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<User, List<Roles>> usersWithRoles = userService.findAll()
                .stream()
                .collect(Collectors.
                        toMap(user -> user, user -> userRoleRepository.findUserRoles(user.getId())));

        req.setAttribute("usersWithRoles", usersWithRoles);
        req.setAttribute("USER",Roles.USER);
        req.setAttribute("MODERATOR", Roles.MODERATOR);
        req.setAttribute("ADMIN", Roles.ADMIN);
        req.setAttribute("allRoles",userRoleRepository.findAll());
        req.getRequestDispatcher("/WEB-INF/jsp/users.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long userId = Long.valueOf(req.getParameter("userId"));
        String deleteAction = req.getParameter("action");
        String changeRoleAction = req.getParameter("action");
        boolean delete = deleteAction != null && deleteAction.equals("delete");
        boolean change = changeRoleAction != null && changeRoleAction.equals("change");

        if (delete) {
            favouritesService.removeByUserId(userId);
            userRoleRepository.deleteAllByUserId(userId);
            cookieRepository.deleteAllByUserId(userId);
            userService.deleteById(userId);
        }
        if (change) {
            String moderatorStr = req.getParameter("moderator");
            String adminStr = req.getParameter("admin");
            boolean moderator = moderatorStr != null && moderatorStr.equals("true");
            boolean admin = adminStr != null && adminStr.equals("true");
            List<Roles> userRoles = userRoleRepository.findUserRoles(userId);
            UserRole adminRole = new UserRole(userId,ADMIN_ROLE_ID);
            UserRole moderatorRole = new UserRole(userId,MODERATOR_ROLE_ID);
            if (userRoles.contains(Roles.MODERATOR) && !moderator){
                userRoleRepository.delete(moderatorRole);
            }
            if (!userRoles.contains(Roles.MODERATOR) && moderator){
                userRoleRepository.save(moderatorRole);
            }
            if (userRoles.contains(Roles.ADMIN) && !admin){
                userRoleRepository.delete(adminRole);
            }
            if (!userRoles.contains(Roles.ADMIN) && admin){
                userRoleRepository.save(adminRole);
            }

        }
    }
}
