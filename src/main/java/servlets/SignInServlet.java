package servlets;

import forms.SignInForm;
import repositories.CookieRepository;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/signIn")
public class SignInServlet extends HttpServlet {
    private UserService userService;
    private CookieRepository cookieRepository;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/signIn.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        userService = (UserService) request.getServletContext().getAttribute("userService");
        cookieRepository = (CookieRepository) request.getServletContext().getAttribute("cookieRepository");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        Optional<Cookie> cookieCandidate = userService.signIn(new SignInForm(username, password));

        if (cookieCandidate.isPresent()){
            Cookie cookie = cookieCandidate.get();
            session.setAttribute(cookie.getName(),cookie.getValue());
            cookieRepository.save(cookie.getValue(),userService.findIdByUsername(username).get());
            response.sendRedirect("/main");
        }else {
            request.setAttribute("error", true);
            request.getRequestDispatcher("/WEB-INF/jsp/signIn.jsp").forward(request,response);
        }

    }
}
