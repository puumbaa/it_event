<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/clearfix.css">
  <link rel="stylesheet" href="../css/clearfix.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
        integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/nav.css">
  <link rel="stylesheet" href="../css/nav.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/addEvent.css">
  <link rel="stylesheet" href="../css/addEvent.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/footer.css">
  <link rel="stylesheet" href="../css/footer.css">

  <title>Редактирование</title>
</head>
<body>
<header class="header sticky-top bg-light">
  <div class="container">
    <nav id="navbar" class="navbar navbar-light bg-light">
      <a class="navbar-brand" href="<c:url value="/main"/>">It event</a>


      <ul class="navbar-nav mr-auto mt-2 mt-lg-0" id="navbar-items" style="width: 35vw">


        <c:if test="${isAdmin==true || isModerator==true}">
          <li class="nav-item">
            <a class="nav-link" href="<c:url value="/processing-events"/>">Проверка</a>
          </li>
        </c:if>


        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/my-events">Мои мероприятия</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="${pageContext.request.contextPath}/addEvent">Добавить мероприятие</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<c:url value="/favourites"/>">Избранные</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="#">Рассылка</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<c:url value="/signIn"/>">Выйти</a>
        </li>

      </ul>
    </nav>
  </div>
</header>

<div class="container" style="padding: 8vh 8vw 0 8vw;">

  <h2>Редактирование данных о мероприятии</h2>

  <form action="${pageContext.request.contextPath}/edit-event" method="post" enctype="multipart/form-data">

    <input type="hidden" value="${event.id}" name="eventId">
    <div class="items">

      <div class="item">
        <label>
          <span>Название мероприятия</span>
          <input type="text" name="title" value="${event.title}" class="info-input" placeholder="Java day" required >
        </label>

        <label>
          <span>Тип</span>
          <input type="text" name="type" value="${event.type}" class="info-input" placeholder="Конференция/Саммит/Митап" required>
        </label>

        <label>
          <span>Темы</span>
          <input type="text" name="themes" value="${event.themes}" class="info-input" placeholder="Java, IOS, UX/UI">
        </label>

        <label>
          <span>Организаторы</span>

          <input type="text" name="organizers" value="${event.organizers}" class="info-input" placeholder="ООО Рога и копыта" required>
        </label>

        <label>
          Изображение
          <input type="file" id="file" name="file" placeholder="Выберите файл">
        </label>

      </div>

      <div class="item">
        <label>
          <span>Дата и время начала:</span>

          <input type="datetime-local" class="info-input" value="${event.startDateTime}" name="startDateTime" required>
        </label>

        <label>
          <span>Дата окончания</span>

          <input type="date"  class="info-input" value="${event.endDate}" name="endDate">
        </label>

        <label>
          <span>Место проведения</span>

          <input type="text" name="location" value="${event.location}" class="info-input" placeholder="Город, улица, дом / Онлайн" required>
        </label>


        <label>
          <span>Официальный сайт мероприятия</span>

          <input type="text" name="link" class="info-input" value="${event.link}" placeholder="Здесь могла бы быть ваша ссылка..." required>
        </label>
      </div>
    </div>

    <div class="description-wrapper">
      <label>
        <span>Описание</span>

        <textarea name="description" id="description" class="info-input" cols="100" rows="10" required>
          ${event.description}
        </textarea>
      </label>
    </div>

    <div class="delimeter"></div>
    <button type="submit" id="send-to-moderation-btn" class="btn btn-primary">Отредактировать</button>
    <form action="/my-events?${event.id}">
      <button type="submit" class="btn btn-secondary">
        Назад
      </button>
    </form>
  </form>
</div>

<footer class="site-footer">
  <div class="container">

    <div class="row" style="justify-content: space-between;">
      <div class="col-sm-12 col-md-6">
        <h6>About</h6>
        <p class="text-justify">Scanfcode.com <i>CODE WANTS TO BE SIMPLE </i> is an initiative  to help the
          upcoming programmers with the code. Scanfcode focuses on providing the most efficient code or
          snippets as the code wants to be simple. We will help programmers build up concepts in different
          programming languages that include C, C++, Java, HTML, CSS, Bootstrap, JavaScript, PHP, Android,
          SQL and Algorithm.</p>
      </div>

      <div class="col-xs-6 col-md-3">
        <h6>Quick Links</h6>
        <ul class="footer-links">
          <li><a href="#">About Us</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Contribute</a></li>
          <li><a href="#">Privacy Policy</a></li>
        </ul>
      </div>
    </div>
    <hr>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-6 col-xs-12">
        <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by
          <a href="#">It event</a>.
        </p>
      </div>

    </div>
  </div>
</footer>

</body>
</html>
