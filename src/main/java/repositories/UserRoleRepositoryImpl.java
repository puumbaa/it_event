package repositories;

import enums.Roles;
import models.User;
import models.UserRole;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class UserRoleRepositoryImpl implements UserRoleRepository {

    //language=sql
    private static final String SQL_DELETE = "delete from user_role where id_user =? and id_role = ?;";

    //language=sql
    private static final String SQL_DELETE_ALL_BY_USER_ID = "delete from user_role where id_user = ?;";

    private final JdbcTemplate jdbcTemplate;
    //language=sql
    private static final String SQL_FIND_ALL = "select * from role";

    //language=sql
    private static final String SQL_INSERT_USER_ROLE = "insert into user_role(id_user, id_role) VALUES (?,?) on conflict do nothing;";

    //language=sql
    private static final String SQL_FIND_USER_ROLES =
                    "select * from user_role as u_r " +
                    "join role r " +
                    "on u_r.id_role = r.id and u_r.id_user = ?";

    private static final RowMapper<Roles> roleMapper = (row, rowNum) -> Roles.valueOf(row.getString("role"));
    @Override
    public boolean save(UserRole userRole) {
        return jdbcTemplate.update(SQL_INSERT_USER_ROLE,userRole.getUserId(),userRole.getRoleId())==1;
    }

    @Override
    public List<Roles> findUserRoles(Long userId) {
        return jdbcTemplate.query(SQL_FIND_USER_ROLES, roleMapper,userId);
    }

    public UserRoleRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Roles> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL,roleMapper);
    }

    @Override
    public void deleteAllByUserId(Long userId) {
        jdbcTemplate.update(SQL_DELETE_ALL_BY_USER_ID,userId);
    }

    @Override
    public void delete(UserRole userRole) {
        jdbcTemplate.update(SQL_DELETE,userRole.getUserId(),userRole.getRoleId());
    }
}
