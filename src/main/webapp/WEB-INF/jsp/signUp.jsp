<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sign Up</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/clearfix.css">
    <link rel="stylesheet" href="../css/clearfix.css">
    <link rel="stylesheet" href="../css/signUp.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signUp.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,700">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


</head>
<body>


<div class="signup-form">
    <form action="${pageContext.request.contextPath}/signUp" method="post" onsubmit="return validateForm()" class="signUpForm" id="signUpForm">
        <h2>Регистрация</h2>
        <p class="lead">Это бесплатно и займет не более 1 минуты.</p>

        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" id="username" name="username" placeholder="Имя пользователя"
                       required  minlength="5" maxlength="32">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-paper-plane"></i></span>
                <input type="email" class="form-control" id="email" name="email" placeholder="Электронная почта"
                       required maxlength="255" onblur="validateEmail()">
                <div id="emailMessage" class="message"></div>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" onkeyup="checkPasswordConfirmation()" id="password" name="password" placeholder="Пароль"
                       required minlength="6">
            </div>
        </div>

        <div class="form-group">
            <div class="input-group">
				<span class="input-group-addon">
					<i class="fa fa-lock"></i>
					<i class="fa fa-check"></i>
				</span>
                <input type="password"  class="form-control" id="confirmPassword" name="passwordConfirm"
                       placeholder="Повторите пароль" onkeyup="checkPasswordConfirmation()" required min="6">
                <div id="passMessage" class="message"></div>
            </div>
        </div>

        <div class="form-group">
            <input type="submit" name="subm" id="submit" class="btn btn-primary btn-block btn-lg" value="Зарегистрироваться"/>
        </div>
        <div class="text-center">
            Уже есть аккаунт?
            <a href="${pageContext.request.contextPath}/signIn">Войти</a>.
        </div>
    </form>

    <c:if test="${emailError}">
        <script type="text/javascript">alert("Электронная почта уже занята")</script>
    </c:if>
    <c:if test="${usernameError}">
        <script type="text/javascript">alert("Имя пользователя уже используется")</script>
    </c:if>
</div>

<script type="text/javascript">
    function checkPasswordConfirmation() {
        let mes = document.getElementById('passMessage');
        if (!isPasswordsSame()) {
            mes.innerHTML = 'Пароли не совпадают';
        }else {
            mes.innerHTML = '';
        }
    }

    function isPasswordsSame() {
        let password = document.getElementById('password').value;
        let confirmPassword = document.getElementById('confirmPassword').value;
        return password === confirmPassword;
    }

    function validateEmail(){
        let mes = document.getElementById('emailMessage');
        if(!isEmailValid()) {
            mes.innerHTML = 'Электронная почта некорректная';
        }else{
            mes.innerHTML = '';
        }
    }

    function isEmailValid(){
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let email = document.getElementById('email').value;
        return email.match(re);
    }

    function validateForm() {
        if(!isPasswordsSame()){
            alert('Пароли не совпадают');
            return false;
        }
        if(!isEmailValid()){
            alert('Электронная почта некорректная');
            return false;
        }
        return true;
    }
</script>

</body>
</html>