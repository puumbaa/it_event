package filters;

import enums.Roles;
import models.User;
import repositories.UserRoleRepository;
import services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebFilter(urlPatterns = {"/main","/event/*"})
public class MainFilter implements Filter {

    private UserService userService;

    private UserRoleRepository userRoleRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext context = filterConfig.getServletContext();
        userService = (UserService) context.getAttribute("userService");
        userRoleRepository = (UserRoleRepository) context.getAttribute("userRoleRepository");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpSession session = request.getSession(false);

        String auth = (String)request.getSession().getAttribute("auth");
        if (session == null || auth == null){
            request.setAttribute("isGuest",true);
        } else {
            User user = userService.findByCookie(auth);
            List<Roles> userRoles = userRoleRepository.findUserRoles(user.getId());

            boolean isUser = userRoles.contains(Roles.USER);
            boolean isAdmin = userRoles.contains(Roles.ADMIN);
            boolean isModerator = userRoles.contains(Roles.MODERATOR);

            request.setAttribute("isUser",isUser);
            request.setAttribute("isAdmin",isAdmin);
            request.setAttribute("isModerator",isModerator);
            request.setAttribute("email",user.getEmail());
            request.setAttribute("isGuest",false);
            request.setAttribute("userId",user.getId());
        }
        filterChain.doFilter(request,response);
    }

    @Override
    public void destroy() {

    }
}
