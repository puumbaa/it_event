package enums;

public enum Roles {
    ADMIN,
    USER,
    MODERATOR;

    @Override
    public String toString() {
        return this.name();
    }
}
