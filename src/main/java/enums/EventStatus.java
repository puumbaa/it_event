package enums;

public enum EventStatus {
    ACTIVE("Активно"),
    PROCESSING("Проверяется"),
    REJECTED("Отклонено");
    private final String translate;
    EventStatus(String translate) {
        this.translate = translate;
    }
    public String translate(){
        return translate;
    }
}
