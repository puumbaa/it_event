package servlets;

import forms.SignUpForm;
import models.UserRole;
import repositories.UserRoleRepository;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {
    private UserService userService;
    private UserRoleRepository userRoleRepository;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        userService = (UserService) request.getServletContext().getAttribute("userService");
        userRoleRepository = (UserRoleRepository) request.getServletContext().getAttribute("userRoleRepository");

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("passwordConfirm");
        String email = request.getParameter("email");

        SignUpForm form = SignUpForm.builder()
                .username(username)
                .password(password)
                .passwordConfirm(passwordConfirm)
                .email(email)
                .build();
        if (userService.signUp(form)) {
            userRoleRepository.save(new UserRole(userService.findIdByUsername(username).get(),1));
            response.sendRedirect("/signIn");
        }else {
            if (userService.findIdByUsername(username).isPresent()){
                request.setAttribute("usernameError",true);
            }
            if (userService.findByEmail(email).isPresent()){
                request.setAttribute("emailError",true);
            }
            request.getRequestDispatcher("/WEB-INF/jsp/signUp.jsp").forward(request,response);
        }
    }
}
