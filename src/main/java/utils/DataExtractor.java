package utils;

import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;

public class DataExtractor {

    public static LocalDateTime extractStartDateTimeForParser(String datetime) {
        final String[] split = datetime.split("\\s");
        String[] date = split[2].split("\\.");


        String[] time = split[3].equals("-") ? split[8].split(":") : split[5].split(":");
        StringBuilder result = new StringBuilder();
        result.append(date[0]).append(".")
        .append(date[1]).append(".")
        .append(date[2]).append(" ")
        .append(time[0]).append(":")
        .append(time[1]);
        return LocalDateTime.parse(result, DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
    }

    public static LocalDate extractEndDateForParser(String datetime) {
        final String[] split = datetime.split("\\s");
        if (split[3].equals("-")) {
            final String[] date = split[4].split("\\.");
            return LocalDate.of(
                    Integer.parseInt(date[2]),
                    Integer.parseInt(date[1]),
                    Integer.parseInt(date[0])
            );
        } else return null;
    }

    public static String extractLocationForParser(String location) {
        int lastCharIndex = location.length() - 1;
        return location.charAt(lastCharIndex) == ',' ? location.substring(18, lastCharIndex) : location.substring(18);
    }

    public static String extractInputValue(Part part){
        try {
            return new BufferedReader(new InputStreamReader(part.getInputStream())).lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
