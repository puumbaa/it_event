<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Вход</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/clearfix.css">
        <link rel="stylesheet" href="../css/clearfix.css">
        <link rel="stylesheet" href="../css/signIn.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signIn.css">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <body>
        <c:if test="${error}">
            <script>
                alert('Вы ввели неправильные данные. Проверьте логин или пароль');
            </script>
        </c:if>

        <div class="signIn-form">
            <form action="<c:url value="/signIn"/>" method="post">
                <h2 class="text-center">Вход</h2>
                <div class="form-group">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" required="required">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required="required">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">Войти</button>
                </div>
                <div class="clearfix">
                    <a href="https://oauth.vk.com/authorize?client_id=7997464&redirect_uri=http://localhost/vk&scope=email&response_type=code&v=5.131" class="vk"><i class="fa fa-vk" aria-hidden="true" style="font-size: 1.5em;"></i></a>
                    <a href="#" class="float-right">Забыли пароль ?</a>
                </div>
                <p class="text-center float-right"><a href="<c:url value="/signUp"/> ">Создать аккаунт</a></p>
            </form>
        </div>

    </body>
</html>