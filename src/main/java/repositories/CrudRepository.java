package repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    boolean save(T t);
    List<T> findAll();
    Optional<T> findById(Long id);
/*    boolean updateById(Long id);
    boolean deleteById(Integer id);*/
}
