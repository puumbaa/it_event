package repositories;

import enums.Roles;
import models.UserRole;

import java.util.List;

public interface UserRoleRepository {
    boolean save(UserRole userRole);
    List<Roles> findUserRoles(Long userId);
    List<Roles> findAll();
    void deleteAllByUserId(Long userId);
    void delete(UserRole userRole);
}
