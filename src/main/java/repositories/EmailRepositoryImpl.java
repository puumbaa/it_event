package repositories;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class EmailRepositoryImpl implements EmailRepository {


    private JdbcTemplate jdbcTemplate;

    public EmailRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<String> emailMapper = (row, rowNum) -> row.getString("email");

    //language=sql
    private static final String SQL_INSERT_INTO_EMAIL = "insert into email (email) values (?) on conflict do nothing;";

    //language=sql
    private static final String SQL_FIND_ALL = "select * from \"email\"";

    //language=sql
    private static final String SQL_DELETE = "delete from \"email\" as e where e.email = ?";

    //language=sql
    private static final String SQL_FIND_BY_EMAIL = "select * from email as e where e.email = ?";


    @Override
    public boolean save(String email) {
        return jdbcTemplate.update(SQL_INSERT_INTO_EMAIL,email) == 1;
    }

    @Override
    public List<String> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL,emailMapper);
    }

    @Override
    public Optional<String> findById(Long id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(String email) {
        if (!jdbcTemplate.query(SQL_FIND_BY_EMAIL,emailMapper,email).isEmpty()){
            jdbcTemplate.update(SQL_DELETE,email);
        }
    }

    @Override
    public Optional<String> findByValue(String value) {
        List<String> emailCandidate = jdbcTemplate.query(SQL_FIND_BY_EMAIL, emailMapper, value);
        if (emailCandidate.isEmpty()) return Optional.empty();
        return Optional.of(emailCandidate.get(0));
    }
}
