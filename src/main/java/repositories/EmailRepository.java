package repositories;

import java.util.List;
import java.util.Optional;

public interface EmailRepository extends CrudRepository<String> {
    void delete(String email);
    Optional<String> findByValue(String email);
}
