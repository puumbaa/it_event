package services;

import enums.EventStatus;
import models.Event;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface EventService {
    boolean save(Event event);
    List<Event> findEventByStatus(EventStatus eventStatus);
    Optional<Event> findById(Long id);
    List<Event> findAllByCreatorId(Long id);
    void updateStatus(EventStatus eventStatus, Long id);
    void setRejectionMessage(String message,Long id);
    void update(String title, String type, String themes, String organizers, LocalDateTime startDateTime, LocalDate endDate, String location, String link, String description, String imageSource, Long id);
    void remove(Long id);
}
