package servlets;

import services.EmailService;
import services.EmailServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/email")
public class EmailServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String subscribe = req.getParameter("subscribe");
        EmailService emailService = (EmailService) req.getServletContext().getAttribute("emailService");
        if (subscribe.equals("true")){
            emailService.save(email);
        }else {
            emailService.delete(email);
        }
    }
}
