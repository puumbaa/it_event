package repositories;

import forms.VkForm;
import models.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserRepositoryImpl implements UserRepository {
    private final JdbcTemplate jdbcTemplate;

    //language=sql
    private static final String SQL_INSERT_FROM_VK = "insert into \"user\" (username,email) values(?, ?) on conflict do nothing;";
    //language=sql
    private static final String SQL_FIND_BY_ID = "select * from \"user\" where id = ?";

    //language=sql
    private static final String SQL_FIND_BY_EMAIL = "select * from \"user\" where email = ?";

    //language=sql
    private static final String SQL_INSERT_USER = "insert into \"user\" (username, password_hash, email ) values(?,?,?) on conflict do nothing;";

    //language=sql
    private static final String SQL_FIND_USER_BY_COOKIE = "select * from cookies where cookie = ?";

    //language=sql
    private static final String SQL_FIND_ID_BY_USERNAME = "select id from \"user\" where username = ? ";
    //language=sql
    private static final String SQL_FIND_BY_USERNAME = "select * from \"user\" where username = ?;";

    //language=sql
    private static final String SQL_FIND_ALL = "select * from \"user\"";

    //language=sql
    private static final String SQL_DELETE_BY_ID = "delete from \"user\" where id = ?;";
    public UserRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<User> userMapper = (row, rowNum) -> {
        Long id = row.getLong("id");
        String username = row.getString("username");
        String email = row.getString("email");
        String password = row.getString("password_hash");
        return User.builder()
                .id(id)
                .username(username)
                .email(email)
                .passwordHash(password)
                .build();
    };

    private static final RowMapper<Map<String, Long>> userCookieMapper = (row, rowNum) -> {
        Map<String, Long> map = new HashMap<>();
        map.put(row.getString("cookie"),row.getLong("user_id"));
        return map;
    };

    @Override
    public boolean save(User user) {
        String username = user.getUsername();
        String email = user.getEmail();
        String password = user.getPasswordHash();

        return jdbcTemplate.update(SQL_INSERT_USER, username, password, email) == 1;
    }

    @Override
    public List<User> findAll() {

        return jdbcTemplate.query(SQL_FIND_ALL,userMapper);

    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Optional<User> findByEmail(String email) {
        List<User> users = jdbcTemplate.query(SQL_FIND_BY_EMAIL,userMapper,email);
        if (!users.isEmpty()) {
            return Optional.of(users.get(0));
        }
        return Optional.empty();
    }


    @Override
    public Optional<User> findByUsername(String username) {
        List<User> query = jdbcTemplate.query(SQL_FIND_BY_USERNAME, userMapper, username);
        return query.isEmpty() ? Optional.empty() : Optional.of(query.get(0));
    }

    @Override
    public void deleteById(Long id) {
        jdbcTemplate.update(SQL_DELETE_BY_ID,id);
    }

    @Override
    public Optional<Long> getIdByUsername(String username) {
        return Optional.ofNullable(jdbcTemplate.query(
                SQL_FIND_ID_BY_USERNAME,
                resultSet -> {
                    if (resultSet.next()) {
                        return resultSet.getLong("id");
                    } else {
                        return null;
                    }
                },
                username));
    }

    @Override
    public User findUserByCookie(String cookie) {
        Long id = jdbcTemplate.query(SQL_FIND_USER_BY_COOKIE, userCookieMapper, cookie).get(0).get(cookie);
        return jdbcTemplate.query(SQL_FIND_BY_ID, userMapper, id).get(0);
    }

    @Override
    public void saveFromVk(VkForm form) {
        jdbcTemplate.update(SQL_INSERT_FROM_VK,form.getUsername(),form.getEmail());
    }
}
