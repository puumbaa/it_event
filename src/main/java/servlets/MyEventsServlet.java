package servlets;

import models.Event;
import models.User;
import repositories.UserRepository;
import services.EventService;
import services.FavouritesService;
import services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@WebServlet("/my-events")
public class MyEventsServlet extends HttpServlet {
    private UserService userService;
    private EventService eventService;
    private FavouritesService favouritesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        userService = (UserService) context.getAttribute("userService");
        eventService = (EventService) context.getAttribute("eventService");
        favouritesService = (FavouritesService) context.getAttribute("favouritesService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String auth = (String) request.getSession().getAttribute("auth");
        String eventId = request.getParameter("id");
        User user = userService.findByCookie(auth);
        Long id = user.getId();

        if (eventId == null || (!eventService.findById(Long.valueOf(eventId)).get().getCreatorId().equals(id))) {
            List<Event> events = eventService.findAllByCreatorId(id);
            request.setAttribute("events", events);
            request.getRequestDispatcher("/WEB-INF/jsp/myEvents.jsp").forward(request, response);
        } else if (eventService.findById(Long.valueOf(eventId)).get().getCreatorId().equals(id)) {
            Event event = eventService.findAllByCreatorId(id).get(0);
            request.setAttribute("event", event);
            request.getRequestDispatcher("/WEB-INF/jsp/myEvent.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Long eventId = Long.valueOf(request.getParameter("eventId"));
        String deleteStr = request.getParameter("delete");
        boolean delete = deleteStr != null && deleteStr.equals("true");
        String isEventPageStr = request.getParameter("isEventPage");
        boolean isEventPage = isEventPageStr != null && isEventPageStr.equals("true");
        if (delete) {
            favouritesService.removeByEventId(eventId);
            eventService.remove(eventId);
        }
        if (isEventPage) {
            request.getRequestDispatcher("/WEB-INF/jsp/myEvents.jsp").forward(request, response);
        }
    }
}
