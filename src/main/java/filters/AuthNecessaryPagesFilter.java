package filters;

import enums.Roles;
import models.User;
import repositories.UserRoleRepository;
import services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebFilter({"/favourites","/addEvent","/my-events"})
public class AuthNecessaryPagesFilter implements Filter {
    private UserService userService;
    private UserRoleRepository userRoleRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext context = filterConfig.getServletContext();
        userService = (UserService) context.getAttribute("userService");
        userRoleRepository = (UserRoleRepository) context.getAttribute("userRoleRepository");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        HttpSession session = request.getSession(false);

        if (session == null || session.getAttribute("auth")==null){
            request.getRequestDispatcher("/WEB-INF/jsp/signIn.jsp").forward(request,response);
        }else {
            User user = userService.findByCookie((String) session.getAttribute("auth"));
            List<Roles> userRoles = userRoleRepository.findUserRoles(user.getId());
            request.setAttribute("isAdmin",userRoles.contains(Roles.ADMIN));
            request.setAttribute("isModerator",userRoles.contains(Roles.MODERATOR));
            request.setAttribute("email",user.getEmail());
            request.setAttribute("userId",user.getId());
        }
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }
}
