package servlets;

import enums.EventStatus;
import models.Event;
import services.EventService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@WebServlet("/processing-events")
public class ProcessingEventsServlet extends HttpServlet {
    private EventService eventService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        eventService = (EventService) context.getAttribute("eventService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        if (id==null) {
            List<Event> events = eventService.findEventByStatus(EventStatus.PROCESSING);
            request.setAttribute("events", events);
            request.getRequestDispatcher("/WEB-INF/jsp/processingEvents.jsp").forward(request, response);
        }else {
            Optional<Event> eventCandidate = eventService.findById(Long.valueOf(id));
            eventCandidate.ifPresent(event -> request.setAttribute("event", event));
            request.getRequestDispatcher("/WEB-INF/jsp/processingEvent.jsp").forward(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String accepted = request.getParameter("accepted");
        Long eventId = Long.valueOf(request.getParameter("eventId"));
        if (accepted != null) {
            if (accepted.equals("y")){
                eventService.updateStatus(EventStatus.ACTIVE, eventId);
            }else if (accepted.equals("n")){
                String message = request.getParameter("rejectionMessage");
                eventService.updateStatus(EventStatus.REJECTED,eventId);
                eventService.setRejectionMessage(message,eventId);
            }
        }
        response.sendRedirect("/processing-events");
    }
}
