package repositories;

import models.Favorites;

import java.util.List;

public interface FavouritesRepository  {
    void insert(Favorites favorites);
    void delete(Favorites favorites);
    List<Favorites> findAllByUserId(Long userId);
    void deleteByEventId(Long id);
    void deleteByUserId(Long id);
}
