package services;

import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileServiceImpl implements FileService {

    private final String STORAGE_PATH = "C:\\Users\\user\\IdeaProjects\\ItEvent\\src\\main\\webapp\\images\\";

    @Override
    public void upload(String fileName, InputStream inputStream) {
        try {
            Files.copy(inputStream, Paths.get(STORAGE_PATH + fileName));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
