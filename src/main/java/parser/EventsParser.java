package parser;

import enums.EventStatus;
import models.Event;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import services.EventService;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static utils.DataExtractor.*;

public class EventsParser implements Runnable {

    private final EventService eventService;

    public EventsParser(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public void run() { getUrlOfEvents().forEach(url -> saveEvent(getParsedEventByUrl(url))); }


    private List<String> getUrlOfEvents() {
        Document mainPage = null;
        try {
            mainPage = Jsoup.connect("https://ict2go.ru/events/").get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        final Elements indexEvents = mainPage
                .getElementsByTag("main").first()
                .getElementsByClass("index-events").get(1)
                .children();

        List<String> eventsUrl = new ArrayList<>();
        indexEvents.forEach(element -> eventsUrl.add(element.select("a").get(0).attr("href")));

        return eventsUrl;
    }

    private void saveEvent(Event newEvent) {
        String title = newEvent.getTitle();

        System.out.println(eventService.save(newEvent) ?
                "Event " + title + " parsed at: " + LocalDateTime.now() : "Event " + title + " already exists");
    }

    private Event getParsedEventByUrl(String url) {
        Document eventPage;
        try {
            eventPage = Jsoup.connect("https://ict2go.ru" + url).get();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String title = eventPage.getElementsByTag("h1").first().text();

        final Element mainContent = eventPage.select(".main-content").get(0);
        final Elements typeAndThemes = mainContent
                .getElementsByClass("event-themes")
                .first()
                .getElementsByTag("a");

        List<String> typeAndThemesList = new ArrayList<>();
        typeAndThemes.forEach(element -> typeAndThemesList.add(element.text()));

        String type = typeAndThemesList.get(0);
        StringBuilder themes = new StringBuilder();
        for (int i = 1; i < typeAndThemesList.size(); i++) {
            themes.append(typeAndThemesList.get(i));
            if (i < typeAndThemesList.size() - 1) themes.append(", ");
        }

        String prefix = "https://ict2go.ru";
        Elements imageCandidate = mainContent.select(".event-info > .image-link > img");
        String imageSource = imageCandidate.isEmpty() ?
                        "C:\\Users\\user\\IdeaProjects\\ItEvent\\src\\main\\resources\\itEventLogo.png"
                        : prefix + imageCandidate.get(0).attr("src");


        final LocalDateTime startDateTime = extractStartDateTimeForParser(eventPage.select(".date-info").get(0).text());
        final LocalDate endDate = extractEndDateForParser(eventPage.select(".date-info").get(0).text());
        final String location = extractLocationForParser(eventPage.select(".place-info").get(0).text());


        Elements linkCandidate = eventPage.select(".event-links > a");
        final String link = linkCandidate.isEmpty() ? "#" : linkCandidate.get(0).attr("href");


        final String organizers = eventPage.select(".organizers > a").first().text();
        final String description = eventPage.select(".description-info").first().text();

        return Event.builder()
                .title(title)
                .imageSource(imageSource)
                .type(type)
                .themes(themes.toString())
                .startDateTime(startDateTime)
                .endDate(endDate)
                .location(location)
                .organizers(organizers)
                .link(link)
                .description(description)
                .creatorId(null)
                .eventStatus(EventStatus.ACTIVE)
                .build();
    }
}



