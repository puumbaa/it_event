package services;

import models.Favorites;
import repositories.FavouritesRepository;

import java.util.List;

public class FavouritesServiceImpl implements FavouritesService {


    private FavouritesRepository favouritesRepository;

    public FavouritesServiceImpl(FavouritesRepository favouritesRepository) {
        this.favouritesRepository = favouritesRepository;
    }

    @Override
    public void save(Favorites favorites) {
        favouritesRepository.insert(favorites);
    }

    @Override
    public void remove(Favorites favorites) {
        favouritesRepository.delete(favorites);
    }

    @Override
    public List<Favorites> findAll(Long userId) {
        return favouritesRepository.findAllByUserId(userId);
    }

    @Override
    public void removeByEventId(Long id) {
        favouritesRepository.deleteByEventId(id);
    }

    @Override
    public void removeByUserId(Long id) {
        favouritesRepository.findAllByUserId(id);
    }
}

