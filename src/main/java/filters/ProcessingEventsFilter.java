package filters;

import enums.Roles;
import models.User;
import repositories.UserRoleRepository;
import services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebFilter("/processing-events")
public class ProcessingEventsFilter implements Filter {

    private UserService userService;

    private UserRoleRepository userRoleRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext context = filterConfig.getServletContext();
        userService = (UserService) context.getAttribute("userService");
        userRoleRepository = (UserRoleRepository) context.getAttribute("userRoleRepository");
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();
        if (session == null || session.getAttribute("auth") == null) {
            request.getRequestDispatcher(request.getContextPath() + "/signIn").forward(request, response);
        } else if (session.getAttribute("auth") != null) {
            String auth = (String) session.getAttribute("auth");
            User user = userService.findByCookie(auth);
            List<Roles> userRoles = userRoleRepository.findUserRoles(user.getId());
            if (!userRoles.contains(Roles.MODERATOR) && !userRoles.contains(Roles.ADMIN)) {
                request.getRequestDispatcher(request.getContextPath() + "/signIn").forward(request, response);
            }
            request.setAttribute("email",user.getEmail());

        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
