package repositories;

import models.Favorites;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class FavouritesRepositoryImpl implements FavouritesRepository {

    private JdbcTemplate jdbcTemplate;

    //language=sql
    private static final String SQL_DELETE_ALL_BY_USER_ID = "delete from favorites where id_user = ?;";

    //language=sql
    private static final String SQL_INSERT_FAVOURITES = "insert into favorites (id_user, id_event) values (?, ?) on conflict (id_user,id_event) do nothing;";

    //language=sql
    private static final String SQL_DELETE_FAVOURITES = "delete from favorites where id_user = ? and id_event = ?;";

    //language=sql
    private static final String SQL_SELECT_ALL_BY_USER_ID = "select * from favorites where id_user = ?;";

    //language=sql
    private static final String SQL_DELETE_BY_EVENT_ID = "delete from favorites where id_event = ?";

    private static final RowMapper<Favorites> mapper = (row, rowNum) -> {
        Long userId = row.getLong("id_user");
        Long eventId = row.getLong("id_event");
        return new Favorites(userId,eventId);
    };

    public FavouritesRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void insert(Favorites favorites) {
        jdbcTemplate.update(SQL_INSERT_FAVOURITES,favorites.getUserId(),favorites.getEventId());
    }

    @Override
    public void delete(Favorites favorites) {
        jdbcTemplate.update(SQL_DELETE_FAVOURITES,favorites.getUserId(), favorites.getEventId());
    }

    @Override
    public List<Favorites> findAllByUserId(Long userId) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_USER_ID,mapper,userId);
    }

    @Override
    public void deleteByEventId(Long id) {
        jdbcTemplate.update(SQL_DELETE_BY_EVENT_ID,id);
    }

    @Override
    public void deleteByUserId(Long id) {
        jdbcTemplate.update(SQL_DELETE_ALL_BY_USER_ID,id);
    }
}
