package repositories;

import forms.SignUpForm;
import forms.VkForm;
import models.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User>{
    Optional<User> findByUsername(String username);
    Optional<Long> getIdByUsername(String userName);
    User findUserByCookie(String cookie);
    Optional<User> findById(Long id);
    Optional<User> findByEmail(String email);
    void saveFromVk(VkForm form);
    void deleteById(Long id);
}
