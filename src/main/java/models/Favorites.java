package models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Favorites {
    private Long userId;
    private Long eventId;
}
