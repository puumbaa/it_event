package repositories;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class CookieRepositoryImpl implements CookieRepository {
    private final JdbcTemplate jdbcTemplate;

    //language=sql
    private static final String SQL_DELETE_ALL_BY_USER_ID = "delete from cookies where user_id = ?;";

    public CookieRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    //language=sql
    private static final String SQL_INSERT_COOKIE = "insert into cookies(cookie, user_id) VALUES (?,?)";

    @Override
    public boolean save(String cookie, Long userId) {
        return jdbcTemplate.update(SQL_INSERT_COOKIE,cookie,userId)==1;
    }

    @Override
    public void deleteAllByUserId(Long userId) {
        jdbcTemplate.update(SQL_DELETE_ALL_BY_USER_ID,userId);
    }
}
