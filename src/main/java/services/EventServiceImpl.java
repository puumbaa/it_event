package services;

import emailSender.HtmlEmailSender;
import enums.EventStatus;
import models.Event;
import repositories.EmailRepositoryImpl;
import repositories.EventRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class EventServiceImpl implements EventService {

    private EventRepository eventRepository;

    private EmailService emailService;


    public EventServiceImpl(EventRepository eventRepository, EmailService emailService) {
        this.eventRepository = eventRepository;
        this.emailService = emailService;
    }

    public EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public boolean save(Event event) {
        boolean success = eventRepository.save(event);
        if (success && event.getEventStatus().equals(EventStatus.ACTIVE)) {
            new Thread(new HtmlEmailSender(emailService, event)).start();
        }
        return success;
    }

    @Override
    public List<Event> findEventByStatus(EventStatus eventStatus) {
        return eventRepository.findEventByStatus(eventStatus);
    }

    @Override
    public Optional<Event> findById(Long id) {
        return eventRepository.findById(id);
    }

    @Override
    public List<Event> findAllByCreatorId(Long id) {
        return eventRepository.findAllByCreatorId(id);
    }

    @Override
    public void updateStatus(EventStatus eventStatus, Long id) {

        eventRepository.changeStatus(eventStatus, id);
        Optional<Event> eventCandidate = eventRepository.findById(id);
        if (eventCandidate.isPresent() && eventCandidate.get().getEventStatus().equals(EventStatus.ACTIVE)) {
            new Thread(new HtmlEmailSender(emailService, eventCandidate.get())).start();
        }

    }

    @Override
    public void setRejectionMessage(String message, Long id) {
        eventRepository.updateRejectionMessage(message, id);
    }

    @Override
    public void update(String title, String type, String themes, String organizers,
                       LocalDateTime startDateTime, LocalDate endDate, String location,
                       String link, String description, String imageSource, Long id) {
        eventRepository.updateEvent(title, type, themes, organizers, startDateTime, endDate, location, link, description, imageSource, id);
    }

    @Override
    public void remove(Long id) {
        eventRepository.deleteEventById(id);
    }
}
