package repositories;

import enums.EventStatus;
import models.Event;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface EventRepository extends CrudRepository<Event>{
    void deleteEventById(Long id);
    List<Event> findAllByCreatorId(Long id);
    List<Event> findEventByStatus(EventStatus eventStatus);
    void changeStatus(EventStatus eventStatus, Long eventId);
    void updateRejectionMessage(String message,Long eventId);
    void updateEvent(String title, String type, String themes, String organizers, LocalDateTime startDateTime,
                     LocalDate endDate, String location, String link, String description, String imageSource,Long id);
}
