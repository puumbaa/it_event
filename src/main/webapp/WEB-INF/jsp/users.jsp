<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>It events</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/clearfix.css">
    <link rel="stylesheet" href="../css/clearfix.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/nav.css">
    <link rel="stylesheet" href="../css/nav.css">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/footer.css">
    <link rel="stylesheet" href="../css/footer.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css"
          integrity="sha512-ioRJH7yXnyX+7fXTQEKPULWkMn3CqMcapK0NNtCN8q//sW7ZeVFcbMJ9RvX99TwDg6P8rAH2IqUSt2TLab4Xmw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>


    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/5.1.1/css/bootstrap.min.css">
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/5.1.1/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

</head>

<body>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
     aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Подтвердите действие</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Отменить действие будет невозможно</p>
            </div>

            <div class="modal-footer">
                <input type="hidden" name="userId" id="userId_">
                <button type="submit" onclick="deleteUser()" class="btn btn-danger">Удалить</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Назад</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mailingModal" tabindex="-1" role="dialog" aria-labelledby="mailingModalLabel"
     aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mailingModalLabel">Управление рассылкой</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <p>Подпишитесь на обновления, чтобы не пропустить события из мира it</p>
                <label for="email" style="display: flex; align-items: center;">
                    <h6>Укажите свою почту</h6>
                    <input type="email" name="email" id="email"
                           style="border: 1px solid #212529; padding: .5vh .5vw; margin-left:1vw; ">
                </label>
                <h6 style="margin-top: 2vh">Выберите действие</h6>
                <div class="radio-inputs" style="display: flex; width: 45%; justify-content: space-between;">
                    <label>
                        Подписаться
                        <input type="radio" name="choose" id="subscribe" value="y" checked style="margin-left: .3vw">
                    </label>
                    <label>
                        Отписаться
                        <input type="radio" name="choose" value="n" style="margin-left: .3vw">
                    </label>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" onclick="sendEmail(this)">Отправить</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Назад</button>
            </div>

        </div>
    </div>
</div>

<header class="header sticky-top bg-light">
    <div class="container">
        <nav id="navbar" class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="<c:url value="/main"/>">It event</a>

            <ul class="navbar-nav mr-auto mt-2 mt-lg-0" id="navbar-items" style="width: 40vw;">

                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/processing-events"/>">Проверка</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/my-events"/>">Мои мероприятия</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/addEvent"/>">Добавить мероприятие</a>
                </li>


                <li class="nav-item" style="background:#F8F9FA;">
                    <button type="button" class="btn nav-link" data-bs-toggle="modal" data-bs-target="#mailingModal"
                            data-value="${email}" onclick="showMailing(this)">
                        Рассылка
                    </button>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/favourites"/>">Избранные</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<c:url value="/signIn"/>">Выйти</a>
                </li>


            </ul>
        </nav>
    </div>
</header>

<h2 class="main-title">Ближайшие мероприятия</h2>
<div class="container" style="padding: 0 8vw;">

    <div class="row">

        <c:forEach items="${usersWithRoles}" var="entry">
            <div class="event-item" id="event-item-${entry.key.id}">

                <div class="event-item-block-1">
                    <input type="hidden" value="${entry.key.id}" id="userId">

                    <div class="event-briefly-info">
                        <h3>${entry.key.username}</h3>

                        <p class="lead event-date">
                                ${entry.key.email}
                        </p>

                        <p class="lead event-time roles" id="${entry.key.id}-roles">
                            Роли: ${entry.value}
                        </p>
                    </div>
                </div>

                <div class="event-item-block">
                    <div>
                        <p>Назначить роль:</p>
                        <c:forEach items="${allRoles}" var="role">
                            <c:if test="${!role.equals(USER)}">
                            <div>
                                <input type="checkbox" class="userRoles" name="${role.name()}" value="${role.name()}"
                                <c:if test="${entry.value.contains(role)}"> checked </c:if>>
                                <label for="${role.name()}">${role.name()}</label>
                            </div>
                            </c:if>
                        </c:forEach>
                    </div>

                    <button onclick="changeRole(this)" id="${entry.key.id}-save-btn" data-id="${entry.key.id}"
                            class="btn btn-primary">
                        Сохранить
                    </button>

                    <button type="button" class="btn btn-danger delete-btn" data-bs-toggle="modal"
                            data-bs-target="#deleteModal" onclick="setHiddenUserData(this)" data-bs-id="${entry.key.id}"
                            data-id="${entry.key.id}"
                            style="margin: 0 2vw;">
                        Удалить
                    </button>
                </div>


            </div>
        </c:forEach>
    </div>
</div>


<footer class="site-footer">
    <div class="container">

        <div class="row" style="justify-content: space-between;">
            <div class="col-sm-12 col-md-6">
                <h6>About</h6>
                <p class="text-justify">Scanfcode.com <i>CODE WANTS TO BE SIMPLE </i> is an initiative to help the
                    upcoming programmers with the code. Scanfcode focuses on providing the most efficient code or
                    snippets as the code wants to be simple. We will help programmers build up concepts in different
                    programming languages that include C, C++, Java, HTML, CSS, Bootstrap, JavaScript, PHP, Android,
                    SQL and Algorithm.</p>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Quick Links</h6>
                <ul class="footer-links">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Contribute</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by
                    <a href="#">It event</a>.
                </p>
            </div>

        </div>
    </div>
</footer>

</body>
</html>
<script>

    function changeRole(button) {
        var userId = button.dataset.id
        var roles = button.parentElement.getElementsByClassName('userRoles')
        var action = "change"
        var moderator = false
        var admin = false
        for (let i = 0; i < roles.length; i++) {
            if ($(roles[i]).is(':checked')) {
                if (roles[i].value === 'MODERATOR') {
                    moderator = true
                }
                if (roles[i].value === 'ADMIN') {
                    admin = true
                }
            }
        }
        var rolesId = "#"+userId +"-roles";

        $.ajax({
            url: "/users",
            method: "post",
            data: {
                userId: userId,
                action: action,
                moderator: moderator,
                admin: admin
            },
            success: function () {
                $(rolesId).load(location.href + " "+rolesId);
            }

        })
    }


    function sendEmail() {
        var value = document.getElementById('email').value
        var subscribe = $('#subscribe').is(':checked')
        $.ajax({
            url: "/email",
            method: "post",
            data: {
                email: value,
                subscribe: subscribe
            },
            success: function () {
                $('#mailingModal').modal('hide');
            }
        })
    }

    function setHiddenUserData(button) {
        var userId = button.dataset.id
        document.getElementById('userId_').setAttribute('value', userId)
    }

    function deleteUser() {

        var delButtons = document.getElementsByClassName('delete-btn')
        var userId = document.getElementById('userId_').value
        $.ajax({
            url: "/users",
            method: "post",
            data: {
                userId: userId,
                action: "delete"
            },
            success: function remove() {
                $('#deleteModal').modal('hide')
                for (let i = 0; i < delButtons.length; i++) {
                    if (delButtons[i].dataset.id === userId) {
                        delButtons[i].parentElement.parentElement.remove()
                    }
                }

            }
        });
    }

    function showMailing(button) {
        var value = button.dataset.value
        document.getElementById('email').setAttribute('value', value)
    }


</script>
