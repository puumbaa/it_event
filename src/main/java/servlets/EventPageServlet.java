package servlets;

import models.Event;
import services.EventService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/event/*")
public class EventPageServlet extends HttpServlet {
    private EventService eventService;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        eventService = (EventService) request.getServletContext().getAttribute("eventService");

        StringBuffer url = request.getRequestURL();
        String eventId = url.toString().split("/")[4];
        Long id = Long.valueOf(eventId);

        Optional<Event> eventCandidate = eventService.findById(id);

        if (eventCandidate.isPresent()) {
            request.setAttribute("event", eventCandidate.get());
            request.getRequestDispatcher("/WEB-INF/jsp/eventPage.jsp").forward(request,response);
        }else{
            //todo: not found page
            request.getRequestDispatcher("/WEB-INF/static/not_found.jsp").forward(request,response);
        }

    }
}
