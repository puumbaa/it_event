package repositories;

public interface CookieRepository {
    boolean save(String cookie, Long userId);
    void deleteAllByUserId(Long userId);

}
