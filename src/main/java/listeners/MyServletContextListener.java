package listeners;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import emailSender.HtmlEmailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import repositories.*;
import services.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class MyServletContextListener implements ServletContextListener {

    private static final String PROPERTIES_PATH = "C:\\Users\\user\\IdeaProjects\\ItEvent\\src\\main\\resources\\application.properties";
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();

        Properties properties = new Properties();
        try {
            properties.load(new FileReader(PROPERTIES_PATH));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        try {
            Class.forName(properties.getProperty("db.driver"));
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setDriverClassName(properties.getProperty("db.driver"));
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);

        DataSource dataSource = new HikariDataSource(config);
        EmailRepository emailRepository = new EmailRepositoryImpl(dataSource);
        EmailService emailService = new EmailServiceImpl(emailRepository);

        context.setAttribute("properties",properties);
        context.setAttribute("emailService", emailService);
        context.setAttribute("userService", new UserServiceImpl(new UserRepositoryImpl(dataSource), new BCryptPasswordEncoder()));
        context.setAttribute("eventService", new EventServiceImpl(new EventRepositoryImpl(dataSource), emailService));
        context.setAttribute("cookieRepository", new CookieRepositoryImpl(dataSource));
        context.setAttribute("userRoleRepository", new UserRoleRepositoryImpl(dataSource));
        context.setAttribute("fileService", new FileServiceImpl());
        context.setAttribute("favouritesService", new FavouritesServiceImpl(new FavouritesRepositoryImpl(dataSource)));
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
