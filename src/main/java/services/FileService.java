package services;

import java.io.InputStream;

public interface FileService {
    void upload(String fileName, InputStream inputStream);
}
