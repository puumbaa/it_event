package services;

import forms.SignInForm;
import forms.SignUpForm;
import forms.VkForm;
import models.User;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

public interface UserService {
    boolean signUp(SignUpForm form);
//    Cookie signInFromVk(VkForm form);
    Optional<Cookie> signIn(SignInForm form);
    Optional<Long> findIdByUsername(String username);
    User findByCookie(String cookie);
    Optional<User> findByEmail(String email);
    void saveFromVk(VkForm form);
    List<User> findAll();
    void deleteById(Long id);
}
