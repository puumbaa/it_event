package services;

import emailSender.HtmlEmailSender;
import repositories.EmailRepository;

import java.util.List;
import java.util.Optional;

public class EmailServiceImpl implements EmailService{
    private EmailRepository emailRepository;

    public EmailServiceImpl(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
    }

    @Override
    public void save(String value) {
        emailRepository.save(value);
    }

    @Override
    public void delete(String value) {
        emailRepository.delete(value);
    }

    @Override
    public List<String> findAll() {
        return emailRepository.findAll();
    }

    @Override
    public Optional<String> findByValue(String value) {
        return emailRepository.findByValue(value);
    }
}
