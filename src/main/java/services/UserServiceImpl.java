package services;

import forms.SignInForm;
import forms.SignUpForm;
import forms.VkForm;
import models.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import repositories.UserRepository;

import javax.servlet.http.Cookie;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    @Override
    public boolean signUp(SignUpForm form) {
        boolean f1 = form.isValidForm();
        boolean f2 = !userRepository.findByUsername(form.getUsername()).isPresent();
        boolean f3 = userRepository.save(User.builder()
                        .username(form.getUsername())
                        .email(form.getEmail())
                        .passwordHash(passwordEncoder.encode(form.getPassword()))
                        .build());

        return f1 && f2 && f3;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<Cookie> signIn(SignInForm form) {
        String username = form.getUsername();
        String password = form.getPassword();
        final Optional<User> userCandidate = userRepository.findByUsername(username);
        if (userCandidate.isPresent()) {
            User user = userCandidate.get();
            if (passwordEncoder.matches(password, user.getPasswordHash())) {
                String val = UUID.randomUUID().toString();
                return Optional.of(new Cookie("auth", val));
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<Long> findIdByUsername(String username) {
        return userRepository.getIdByUsername(username);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByCookie(String cookie) {
        return userRepository.findUserByCookie(cookie);
    }

    @Override
    public void saveFromVk(VkForm form) {
        userRepository.saveFromVk(form);
    }
}
