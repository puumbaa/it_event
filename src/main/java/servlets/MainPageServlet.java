package servlets;

import enums.EventStatus;
import models.*;
import services.EventService;
import services.FavouritesService;
import services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/main")
public class MainPageServlet extends HttpServlet {

    private EventService eventService;
    private UserService userService;
    private FavouritesService favouritesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        eventService = (EventService) context.getAttribute("eventService");
        userService = (UserService) context.getAttribute("userService");
        favouritesService = (FavouritesService) context.getAttribute("favouritesService");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();

        //TODO узнать почему у ивентов creatorId = 0 а не null (см. save в парсере и репо)
        List<Event> events = eventService.findEventByStatus(EventStatus.ACTIVE);
        if (!((boolean) request.getAttribute("isGuest"))) {
            User user = userService.findByCookie((String) session.getAttribute("auth"));

            List<Long> keys = favouritesService.findAll(user.getId())
                    .stream()
                    .map(Favorites::getEventId)
                    .collect(Collectors.toList());

            events.forEach(event -> event.setFavourite(keys.contains(event.getId())));
        }
        request.setAttribute("events", events);
        request.getRequestDispatcher("/WEB-INF/jsp/main.jsp").forward(request, response);

    }
}
