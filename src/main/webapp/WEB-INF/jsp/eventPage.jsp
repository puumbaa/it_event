<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${event.title}</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/clearfix.css">
    <link rel="stylesheet" href="../css/clearfix.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/nav.css">
    <link rel="stylesheet" href="../css/nav.css">
    <link rel="stylesheet" href="../css/eventPage.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/eventPage.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/footer.css">
    <link rel="stylesheet" href="../css/footer.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <style>
        .event-image {
            background-image: url("${event.imageSource}");
        }
    </style>
</head>
<body>

<header class="header sticky-top bg-light">
    <div class="container">
        <nav id="navbar" class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="<c:url value="/main"/>">It event</a>


            <ul class="navbar-nav mr-auto mt-2 mt-lg-0" id="navbar-items"
                <c:if test="${isGuest==false}">style="width: 35vw;" </c:if>>


                <c:if test="${isAdmin==true || isModerator==true}">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/processing-events"/>">Проверка</a>
                    </li>
                </c:if>


                <c:if test="${isGuest==false}">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/my-events"/>">Мои мероприятия</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/addEvent"/>">Добавить мероприятие</a>
                    </li>
                </c:if>

                <li class="nav-item">
                    <a class="nav-link" href="#">Рассылка</a>
                </li>

                <c:if test="${isGuest==false}">

                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/favourites"/>">Избранные</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/signIn"/>">Выйти</a>
                    </li>
                </c:if>

                <c:if test="${isGuest==true}">
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/signIn"/>">Войти</a>
                    </li>

                    <li>
                        <a class="nav-link" href="<c:url value="/signUp"/>">Зарегистрироваться</a>
                    </li>
                </c:if>
            </ul>
        </nav>
    </div>
</header>

<div class="wrapper">
    <div class="container" style="padding: 0 8vw;">
        <div class="main-content">

            <div class="event-image"></div>

            <div class="event-main-info">

                <h1 class="event-title">${event.title}</h1>

                <p class="lead event-date">
                    Дата: ${event.getDateOfStart()}
                    <c:if test="${event.endDate!=null}"> - ${event.getDate(event.endDate)} </c:if>
                </p>

                <p class="lead event-time"> Время начала: ${event.getTimeOfStart()} </p>

                <p class="lead event-location">${event.location}</p>

                <p class="event-type lead">${event.type}
                    <c:if test="${event.themes!=null}"> / <span class="lead">Темы:</span> ${event.themes} </c:if>
                </p>

                <p class="organizers lead">Организатор: ${event.organizers}</p>

                <a href="${event.link}">Зарегистрироваться</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="description">
        <h2 class="description-title">Описание</h2>
        <p>${event.description}</p>
    </div>
</div>




<footer class="site-footer">
    <div class="container">

        <div class="row" style="justify-content: space-between;">
            <div class="col-sm-12 col-md-6">
                <h6>About</h6>
                <p class="text-justify">Scanfcode.com <i>CODE WANTS TO BE SIMPLE </i> is an initiative  to help the
                    upcoming programmers with the code. Scanfcode focuses on providing the most efficient code or
                    snippets as the code wants to be simple. We will help programmers build up concepts in different
                    programming languages that include C, C++, Java, HTML, CSS, Bootstrap, JavaScript, PHP, Android,
                    SQL and Algorithm.</p>
            </div>

            <div class="col-xs-6 col-md-3">
                <h6>Quick Links</h6>
                <ul class="footer-links">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Contribute</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by
                    <a href="#">It event</a>.
                </p>
            </div>

        </div>
    </div>
</footer>

</body>
</html>
