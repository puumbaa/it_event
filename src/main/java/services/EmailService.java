package services;

import java.util.List;
import java.util.Optional;

public interface EmailService {
    void save(String value);
    void delete(String value);
    List<String> findAll();
    Optional<String> findByValue(String value);
}
