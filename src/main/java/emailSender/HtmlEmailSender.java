package emailSender;

import models.Event;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import services.EmailService;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Properties;

public class HtmlEmailSender implements Runnable {

    private EmailService emailService;
    private Event event;
    private static final String PROPERTY_PATH = "\"C:\\\\Users\\\\user\\\\IdeaProjects\\\\ItEvent\\\\src\\\\main\\\\resources\\\\application.properties\"";

    public HtmlEmailSender(EmailService emailService, Event event) {
        this.emailService = emailService;
        this.event = event;
    }

    @Override
    public void run() {
        List<String> all = emailService.findAll();
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(PROPERTY_PATH));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        String imageSource;
        if (event.getCreatorId()==0){
            imageSource = event.getImageSource();
        }else {
            imageSource = properties.getProperty("app.domain-name")+"/file-download?filename="+event.getImageName();
        }

        HtmlEmail email = new HtmlEmail();
        email.setHostName("smtp.googlemail.com");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(
                properties.getProperty("email.my-email"),
                properties.getProperty("email.password")));
        email.setSSLOnConnect(true);

        all.forEach(x -> {
            try {
                email.addTo(x);
            } catch (EmailException e) {
                throw new IllegalArgumentException(e);
            }
            try {
                email.setSubject(event.getTitle());
                email.setFrom(properties.getProperty("email.my-email"), "it event");
                URL url = new URL(imageSource);
                String contentId = email.embed(url,"Event logo");
                email.setHtmlMsg(
                                "<html>" +
                                "<h2>" + event.getTitle() + "</h2> " +
                                "<br> " +
                                "<img src = \"cid:" + contentId + "\"/> " +
                                "<br> " +
                                "<div> " + event.getDescription() + " </div> " +
                                "<br> " +
                                "<a href = \"" + event.getLink() + "\"> Зарегистрироваться! </a>" +
                                "</html>"
                );
                email.setTextMsg("Your email client doesn't support HTML messages");
                email.send();
            } catch (EmailException | MalformedURLException ignored) { }
        });
    }
}
